package br.edu.unisep.benefits.controller.account;

import br.edu.unisep.benefits.domain.dto.account.AccountDto;
import br.edu.unisep.benefits.domain.dto.user.UserDto;
import br.edu.unisep.benefits.domain.usecase.account.GetAccountByUserUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/account")
public class AccountController {

    private final GetAccountByUserUseCase getAccountByUser;

    @GetMapping("/{user}")
    public ResponseEntity<AccountDto> findAll(@PathVariable("user") Integer user) {
        var account = getAccountByUser.execute(user);
        return ResponseEntity.ok(account);
    }

}
