package br.edu.unisep.benefits.controller.card;

import br.edu.unisep.benefits.domain.dto.card.CardBalanceDto;
import br.edu.unisep.benefits.domain.dto.card.CardDto;
import br.edu.unisep.benefits.domain.usecase.card.GetBalanceByCardUseCase;
import br.edu.unisep.benefits.domain.usecase.card.GetCardByIdUseCase;
import br.edu.unisep.benefits.domain.usecase.card.GetCardsByAccountUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/card")
public class CardController {

    private final GetCardsByAccountUseCase getCardsByAccount;
    private final GetBalanceByCardUseCase getBalanceByCard;
    private final GetCardByIdUseCase getCardById;

    @GetMapping("/byAccount/{account}")
    public ResponseEntity<List<CardDto>> findByAccount(@PathVariable("account") Integer account) {
        var cards = getCardsByAccount.execute(account);
        return ResponseEntity.ok(cards);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CardDto> findById(@PathVariable("id") Integer id) {
        var card = getCardById.execute(id);
        return ResponseEntity.ok(card);
    }

    @GetMapping("/balance/{card}")
    public ResponseEntity<List<CardBalanceDto>> findBalance(@PathVariable("card") Integer card) {
        var balance = getBalanceByCard.execute(card);
        return ResponseEntity.ok(balance);
    }

}
