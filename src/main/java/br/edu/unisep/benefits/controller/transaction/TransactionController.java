package br.edu.unisep.benefits.controller.transaction;

import br.edu.unisep.benefits.domain.dto.transaction.TransactionDto;
import br.edu.unisep.benefits.domain.usecase.transaction.GetTransactionsByCardUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    private final GetTransactionsByCardUseCase getTransactionsByCard;

    @GetMapping("/{card}")
    public ResponseEntity<List<TransactionDto>> findByCard(@PathVariable("card") Integer card) {
        var transactions = getTransactionsByCard.execute(card);
        return ResponseEntity.ok(transactions);
    }

}
