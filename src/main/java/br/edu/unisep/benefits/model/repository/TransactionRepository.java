package br.edu.unisep.benefits.model.repository;

import br.edu.unisep.benefits.model.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    @Query
    List<Transaction> findByCard_IdOrderByDateDesc(Integer card);

}
