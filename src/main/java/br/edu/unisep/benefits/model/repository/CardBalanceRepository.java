package br.edu.unisep.benefits.model.repository;

import br.edu.unisep.benefits.model.entity.CardBalance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardBalanceRepository extends JpaRepository<CardBalance, Integer> {

    @Query
    List<CardBalance> findByCard_Id(Integer card);

}
