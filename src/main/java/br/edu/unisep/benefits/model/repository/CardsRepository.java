package br.edu.unisep.benefits.model.repository;

import br.edu.unisep.benefits.model.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsRepository extends JpaRepository<Card, Integer> {

    @Query
    List<Card> findByAccount_Id(Integer account);

}
