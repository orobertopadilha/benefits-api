package br.edu.unisep.benefits.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "cards_balance")
public class CardBalance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "balance_id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "card_id")
    private Card card;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "amount")
    private Double amount;

}
