package br.edu.unisep.benefits.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "card_id")
    private Card card;

    @Column(name = "transaction_type")
    private Integer type;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "amount")
    private Double amount;

    @OneToOne
    @JoinColumn(name = "category_from", referencedColumnName = "category_id")
    private Category categoryFrom;

    @OneToOne
    @JoinColumn(name = "category_to", referencedColumnName = "category_id")
    private Category categoryTo;
}
