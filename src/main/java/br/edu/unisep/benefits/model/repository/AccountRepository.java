package br.edu.unisep.benefits.model.repository;

import br.edu.unisep.benefits.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query
    Account findByUser_Id(Integer user);

}
