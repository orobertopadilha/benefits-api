package br.edu.unisep.benefits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BenefitsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BenefitsApiApplication.class, args);
	}

}
