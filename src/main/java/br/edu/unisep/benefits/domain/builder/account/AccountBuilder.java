package br.edu.unisep.benefits.domain.builder.account;

import br.edu.unisep.benefits.domain.dto.account.AccountDto;
import br.edu.unisep.benefits.model.entity.Account;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountBuilder {

    public AccountDto from(Account account) {
        return new AccountDto(
                account.getId(),
                account.getUser().getId(),
                account.getCreationDate());
    }

    public List<AccountDto> from(List<Account> accounts) {
        return accounts.stream().map(this::from).collect(Collectors.toList());
    }

}
