package br.edu.unisep.benefits.domain.usecase.card;

import br.edu.unisep.benefits.domain.builder.card.CardBalanceBuilder;
import br.edu.unisep.benefits.domain.dto.card.CardBalanceDto;
import br.edu.unisep.benefits.model.repository.CardBalanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetBalanceByCardUseCase {

    private final CardBalanceRepository balanceRepository;
    private final CardBalanceBuilder balanceBuilder;

    public List<CardBalanceDto> execute(Integer cardId) {
        var balance = balanceRepository.findByCard_Id(cardId);
        return balanceBuilder.from(balance);
    }

}
