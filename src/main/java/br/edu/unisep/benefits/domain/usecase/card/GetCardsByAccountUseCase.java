package br.edu.unisep.benefits.domain.usecase.card;

import br.edu.unisep.benefits.domain.builder.card.CardBuilder;
import br.edu.unisep.benefits.domain.dto.card.CardDto;
import br.edu.unisep.benefits.model.repository.CardsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetCardsByAccountUseCase {

    private final CardsRepository cardsRepository;
    private final CardBuilder cardBuilder;

    public List<CardDto> execute(Integer account) {
        var cards = cardsRepository.findByAccount_Id(account);
        return cardBuilder.from(cards);
    }

}
