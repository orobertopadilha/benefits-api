package br.edu.unisep.benefits.domain.usecase.account;

import br.edu.unisep.benefits.domain.builder.account.AccountBuilder;
import br.edu.unisep.benefits.domain.dto.account.AccountDto;
import br.edu.unisep.benefits.model.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetAccountByUserUseCase {

    private final AccountRepository accountRepository;
    private final AccountBuilder accountBuilder;

    public AccountDto execute(Integer user) {
        var account = accountRepository.findByUser_Id(user);
        return accountBuilder.from(account);
    }
}
