package br.edu.unisep.benefits.domain.dto.account;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class AccountDto {

    private Integer id;
    private Integer user;

    private LocalDate creationDate;

}
