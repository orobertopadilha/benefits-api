package br.edu.unisep.benefits.domain.usecase.card;

import br.edu.unisep.benefits.domain.builder.card.CardBuilder;
import br.edu.unisep.benefits.domain.dto.card.CardDto;
import br.edu.unisep.benefits.model.repository.CardsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetCardByIdUseCase {

    private final CardsRepository cardsRepository;
    private final CardBuilder cardBuilder;

    public CardDto execute(Integer cardId) {
        var cards = cardsRepository.findById(cardId);
        return cards.map(cardBuilder::from).orElse(null);
    }

}
