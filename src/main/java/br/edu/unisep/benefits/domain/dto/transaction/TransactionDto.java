package br.edu.unisep.benefits.domain.dto.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class TransactionDto {

    private Integer id;

    private String card;

    private Integer type;

    private LocalDate date;

    private Double amount;

    private String categoryFrom;

    private String categoryTo;
}
