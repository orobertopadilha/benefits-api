package br.edu.unisep.benefits.domain.builder.card;

import br.edu.unisep.benefits.domain.dto.card.CardDto;
import br.edu.unisep.benefits.model.entity.Card;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CardBuilder {

    public CardDto from(Card card) {
        return new CardDto(
                card.getId(),
                card.getAccount().getId(),
                card.getNumber(),
                card.getExpiration());
    }

    public List<CardDto> from(List<Card> cards) {
        return cards.stream().map(this::from).collect(Collectors.toList());
    }

}
