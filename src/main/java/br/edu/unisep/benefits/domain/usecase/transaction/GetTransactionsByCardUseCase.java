package br.edu.unisep.benefits.domain.usecase.transaction;

import br.edu.unisep.benefits.domain.builder.transaction.TransactionBuilder;
import br.edu.unisep.benefits.domain.dto.transaction.TransactionDto;
import br.edu.unisep.benefits.model.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GetTransactionsByCardUseCase {

    private final TransactionRepository transactionRepository;
    private final TransactionBuilder transactionBuilder;

    public List<TransactionDto> execute(Integer card) {
        var transactions = transactionRepository.findByCard_IdOrderByDateDesc(card);
        return transactionBuilder.from(transactions);
    }

}
