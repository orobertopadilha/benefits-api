package br.edu.unisep.benefits.domain.builder.transaction;

import br.edu.unisep.benefits.domain.dto.transaction.TransactionDto;
import br.edu.unisep.benefits.model.entity.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TransactionBuilder {

    public TransactionDto from(Transaction transaction) {
        return new TransactionDto(
                transaction.getId(),
                transaction.getCard().getNumber(),
                transaction.getType(),
                transaction.getDate(),
                transaction.getAmount(),
                transaction.getCategoryFrom() != null ? transaction.getCategoryFrom().getName() : null,
                transaction.getCategoryTo().getName());
    }

    public List<TransactionDto> from(List<Transaction> categories) {
        return categories.stream().map(this::from).collect(Collectors.toList());
    }

}
