package br.edu.unisep.benefits.domain.builder.card;

import br.edu.unisep.benefits.domain.dto.card.CardBalanceDto;
import br.edu.unisep.benefits.model.entity.CardBalance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CardBalanceBuilder {

    public CardBalanceDto from(CardBalance balance) {
        return new CardBalanceDto(
                balance.getId(),
                balance.getCard().getId(),
                balance.getCategory().getId(),
                balance.getCategory().getName(),
                balance.getAmount());
    }

    public List<CardBalanceDto> from(List<CardBalance> balances) {
        return balances.stream().map(this::from).collect(Collectors.toList());
    }

}
