package br.edu.unisep.benefits.domain.dto.transaction;

import lombok.Data;

@Data
public class NewTransactionDto {

    private Integer id;

    private Integer card;

    private Integer type;

    private Double amount;

    private Integer categoryFrom;

    private Integer categoryTo;
}
