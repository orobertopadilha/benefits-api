package br.edu.unisep.benefits.domain.dto.card;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CardBalanceDto {

    private Integer id;
    private Integer card;
    private Integer categoryId;
    private String category;
    private Double amount;

}
