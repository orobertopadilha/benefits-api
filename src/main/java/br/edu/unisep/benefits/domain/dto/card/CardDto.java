package br.edu.unisep.benefits.domain.dto.card;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class CardDto {

    private Integer id;

    private Integer account;

    private String number;

    private LocalDate expiration;

}
